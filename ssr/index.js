require('asset-require-hook')({
  extensions: ['svg', 'css', 'less', 'jpg', 'png', 'gif']
});
windowPolyfill();
require('./server');

/**
 * window兼容
 */
function windowPolyfill() {
  const localStorage = require('localStorage');
  global.localStorage = localStorage;
  const { JSDOM } = require('jsdom');
  const { window } = new JSDOM('<!DOCTYPE html><body></body>', {
    url: 'https://www.h5ds.com/'
  });
  global.window = window;
  global.location = window.location;
  global.window.localStorage = localStorage;
  global.document = window.document;
  global.navigator = window.navigator;
  global.self = {};
}
