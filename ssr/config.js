const config = {
  port: 3030, // 服务器端口
  proxy: {
    // 代理请求
    target: 'https://www.h5ds.com',
    changeOrigin: true
  }
};

module.exports = { config };
