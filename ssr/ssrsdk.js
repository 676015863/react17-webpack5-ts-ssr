import domReady from '@wordpress/dom-ready';

export function ssrInit() {
  domReady(function () {
    coverServerDOM();
  });
}

/**
 * 同步本地代码到服务端代码，应该页面初始化就执行此参数
 * 如果做了SSR，那么页面中是存在SSR_STATIC_DATA参数的
 * 初始化的时候销毁服务的返回的数据#ServerApp，然后使用react的#App接管页面
 */
export function coverServerDOM() {
  var appServerTarget = document.getElementById('AppServer');
  if (appServerTarget) {
    document.body.removeChild(appServerTarget);
    document.getElementById('App').style.display = 'block';
  }
}

/**
 * 获取全局的SSRDATA数据
 * @returns
 */
export function getSSRData() {
  if (window.SSR_STATIC_DATA) {
    return window.SSR_STATIC_DATA;
  } else {
    return null;
  }
}

/**
 * 获取ssr data 的 scripts数据 string
 * @param {*} route
 * @returns
 */
export async function injectSSRDataToScripts(route) {
  let scripts = null;
  let component = null;
  if (!route.component.preload) {
    component = route.component;
  } else {
    component = await route.component.preload().then(res => res.default);
  }
  if (component.injectSSRData) {
    const res = await component.injectSSRData(route);
    try {
      console.log('route', route);
      component.defaultProps = { ...res };
      scripts = `<script>window.SSR_STATIC_DATA = ${JSON.stringify(res)};</script>`;
    } catch (e) {
      console.error('组件static ssData 必须返回一个对象');
      return '<h1 style="color:red;">ssData 必须返回一个对象</h1>';
    }
  } else {
    // 如果没有异步数据
    scripts = '';
  }
  return scripts;
}
