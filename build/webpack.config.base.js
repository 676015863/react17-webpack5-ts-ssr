const { resolve, srcPath, hash } = require('./config');
// webpack 配置文档
const CaseSensitivePathsPlugin = require('case-sensitive-paths-webpack-plugin');

module.exports = {
  resolve: {
    modules: ['node_modules', resolve('../node_modules')],
    extensions: ['.js', '.es', '.ts', '.tsx', '.css', '.less'],
    alias: {
      '@components': resolve('../src/components'),
      '@server': resolve('../src/server'),
      '@pages': resolve('../src/pages'),
      '@language': resolve('../src/language'),
      '@hooks': resolve('../src/hooks'),
      '@theme': resolve('../src/theme'),
      '@layout': resolve('../src/layout'),
      '@stores': resolve('../src/stores'),
      '@utils': resolve('../src/utils'),
      '@config': resolve('../src/config'),
      '@less': resolve('../src/less'),
      '@images': resolve('../src/assets/images'),
      '@icons': resolve('../src/icons'),
      '@utils': resolve('../src/utils'),
      '@ssr': resolve('../ssr')
    }
  },
  module: {
    rules: [
      {
        test: /\.(ts|tsx|js)$/,
        include: [srcPath, resolve('../node_modules'), resolve('../ssr')],
        use: ['babel-loader']
      },
      {
        test: /\.(woff|eot|ttf|svg)$/,
        include: srcPath,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 10,
              name: `assets/fonts/[name]${hash}.[ext]`
            }
          }
        ]
      },
      {
        // 图片加载处理
        test: /\.(png|jpg|jpeg|gif|ico|svg)$/,
        include: srcPath,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 1,
              name: `assets/images/[name]${hash}.[ext]`
            }
          }
        ]
      }
      // {
      //   test: /\.html$/,
      //   loader: 'html-loader'
      // }
    ]
  },
  plugins: [new CaseSensitivePathsPlugin()]
};
