const { resolve, srcPath, version, hash, distPath } = require('./config');

const baseConfig = require('./webpack.config.base');
const { merge } = require('webpack-merge');
const FileManagerPlugin = require('filemanager-webpack-plugin');
const nodeExternals = require('webpack-node-externals');
const LessPluginFunctions = require('less-plugin-functions');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const webpack = require('webpack');

module.exports = merge(baseConfig, {
  devtool: false,
  target: 'node',
  externalsPresets: { node: true }, // in order to ignore built-in modules like path, fs, etc.
  externals: [nodeExternals()],
  entry: {
    ssr: resolve('../src/ssr.js') // 主网站入口
  },
  output: {
    publicPath: '/',
    path: distPath,
    filename: `ssr/[name].js`,
    libraryTarget: 'commonjs2'
  },
  // mode: 'production',
  mode: 'development',
  module: {
    rules: [
      {
        test: /\.(css|less)$/,
        include: [srcPath, resolve('../node_modules')],
        use: [
          { loader: 'style-loader' },
          {
            loader: 'css-loader',
            options: { sourceMap: true, modules: { auto: true, localIdentName: '[name]__[local]' } }
          },
          { loader: 'postcss-loader', options: { sourceMap: true } },
          {
            loader: 'less-loader',
            options: {
              lessOptions: {
                // paths: [resolve('../src/less')],
                javascriptEnabled: true,
                plugins: [new LessPluginFunctions()]
              }
            }
          }
        ]
      }
    ]
  },
  plugins: [
    new webpack.DefinePlugin({
      __RUNNING_ENV__: JSON.stringify('ssr')
    }),
    new MiniCssExtractPlugin({
      filename: `ssr/css/[name].${version}${hash}.css`
    }),
    new FileManagerPlugin({
      events: {
        onEnd: {
          copy: [
            { source: resolve('../ssr/server.js'), destination: resolve('../dist/server.js') },
            { source: resolve('../ssr/index.js'), destination: resolve('../dist/index.js') }
          ]
        }
      }
    })
  ]
});
