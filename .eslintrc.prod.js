module.exports = {
  extends: ['.eslintrc.js'],
  rules: {
    'no-console': ['error', { allow: ['error', 'warn'] }],
    'no-debugger': 'error',
    'no-eval': 'error'
  }
};
