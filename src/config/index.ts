// 配置
const config = {
  secretKey: '1QiLCJhx1', // 加密
  apiHost: '/api/v1',
  prefix: 'h5', // 项目前缀，用于设置localStroage的名称
  unLoginLinkTo: '/home', // 访问未登录页面跳转
  loginLinkTo: '/manage', // 登录后页面跳转
  basename: '' // history路由前缀
};

// 生产环境参数
if (process.env.NODE_ENV !== 'development') {
  config.apiHost = '/api/v1';
}

export { config };
