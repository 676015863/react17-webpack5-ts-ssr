import React from 'react';
import styles from './header.module.less';
import { Link } from 'react-router-dom';

export default function Header() {
  return (
    <div className={styles.header}>
      header &nbsp;
      <Link to="/">home</Link>
      &nbsp;
      <Link to="/manage">manage</Link>
    </div>
  );
}
