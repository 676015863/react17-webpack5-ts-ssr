import React, { useEffect, useState } from 'react';
import { Form, Input, Button, Row, Col, Divider } from 'antd';
import { userService } from '@server';
import { withRouter } from 'react-router-dom';
import { pubsub } from '@utils';

function LoginMobile({ setShow, history }) {
  const [captcha, setCaptcha] = useState({
    key: '',
    base64Url: ''
  });

  const onFinish = async values => {
    // 1、登录成功后自动设置x-token
    const res = await userService.login({ ...values, captchaKey: captcha.key });
    if (!res) {
      // 切换验证码
      getImageKey();
      return;
    }
    // 2、获取用户详情，设置x-user-info
    const ress = await userService.getUserDetail();
    if (ress) {
      history.push(location.pathname);
      pubsub.publish('showLoginModal', false);
    }
  };
  useEffect(() => {
    getImageKey();
  }, []);

  const getImageKey = async () => {
    let res = await userService.getCaptcha();
    if (res) {
      setCaptcha({
        base64Url: res.image,
        key: res.key
      });
    }
  };

  return (
    <div className="login-register-mobile">
      <h1 className="login-register-title">账号登录</h1>
      <Form name="basic" onFinish={onFinish}>
        <Form.Item name="username" rules={[{ required: true, message: '请输入手机号或邮箱账号' }]}>
          <Input placeholder="请输入手机号或邮箱账号" prefix={<i className="webfont icow-yonghu" />} />
        </Form.Item>
        <Form.Item name="password" rules={[{ required: true, message: '请输入密码' }]}>
          <Input.Password placeholder="请输入密码" prefix={<i className="webfont icow-mima" />} />
        </Form.Item>
        <Form.Item>
          <Row gutter={8}>
            <Col span={16}>
              <Form.Item name="captchaCode" noStyle rules={[{ required: true, message: '请输入验证码' }]}>
                <Input placeholder="请输入验证码" prefix={<i className="webfont icow-yanzhengma" />} />
              </Form.Item>
            </Col>
            <Col span={8}>
              <img onClick={getImageKey} style={{ width: 86, height: 34 }} src={captcha.base64Url} alt="" />
            </Col>
          </Row>
        </Form.Item>
        <Button type="primary" htmlType="submit" block={true}>
          立即登录
        </Button>
      </Form>
      <div className="login-footer">
        <a onClick={() => setShow('forget_password')}>忘记密码</a>
        <Divider type="vertical" />
        <a onClick={() => setShow('register')}>账号注册</a>
      </div>
    </div>
  );
}

export default withRouter(LoginMobile);
