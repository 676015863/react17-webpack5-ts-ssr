import React, { Component } from 'react';
import { userService } from '@server';
import { withRouter } from 'react-router-dom';
import { pubsub } from '@utils';

@withRouter
class LoginQrcode extends Component {
  constructor(props) {
    super(props);
    this.state = {
      url: '',
      sn: ''
    };
  }

  // 轮询监测登录状态
  seekLogin = async sn => {
    this.timer = setTimeout(async () => {
      const res = await userService.seekWxLogin(sn);
      console.log('seekLogin', res);
      if (res.user_id === '0') {
        this.seekLogin(sn);
      } else {
        clearTimeout(this.timer);
        // 获取到token了
        userService._setRqHeaderToken(res.token);
        // 2、获取用户详情，设置x-user-info
        const ress = await userService.getUserDetail();
        if (ress) {
          this.props.history.push(location.pathname);
          pubsub.publish('showLoginModal', false);
        }
      }
    }, 3000);
  };

  doQrcode = async () => {
    const res = await userService.getWxQrcode();
    console.log('res', res);
    this.setState({
      url: res.url,
      sn: res.sn
    });

    // 轮询登录
    this.seekLogin(res.sn);
  };

  componentDidMount() {
    this.doQrcode();
  }

  componentWillUnmount() {
    if (this.timer) {
      clearTimeout(this.timer);
    }
  }

  render() {
    const { url } = this.state;
    return (
      <div className="login-register-qrcode">
        <p>
          <i className="webfont icow-weixin-full" />
          微信扫描立即登录
        </p>
        <img style={{ width: 200 }} src={url || 'https://cdn.h5ds.com/static/images/qrcode-loading.png'} alt="" />
      </div>
    );
  }
}

export default LoginQrcode;
