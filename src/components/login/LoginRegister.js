import React, { useState, useEffect } from 'react';
import { Modal } from 'antd';
import LoginRegisterBox from './LoginRegisterBox';
import { user } from '@stores/user';
import { withRouter } from 'react-router-dom';
import { pubsub } from '@utils';

function LoginRegister({ children }) {
  const [visible, setVisible] = useState(false);

  const showVisible = () => {
    if (!user.info) {
      setVisible(true);
    } else {
      history.push(location.pathname);
    }
  };

  useEffect(() => {
    pubsub.subscribe('showLoginModal', mark => {
      if (mark !== undefined) {
        setVisible(mark);
      } else {
        setVisible(true);
      }
    });
    return () => {
      pubsub.unsubscribe('showLoginModal');
    };
  }, []);

  return (
    <>
      {children ? <span onClick={showVisible}>{children}</span> : <a onClick={showVisible}>登录/注册</a>}
      <Modal
        bodyStyle={{ padding: 0 }}
        title={null}
        width={800}
        visible={visible}
        zIndex={2000}
        footer={null}
        destroyOnClose={true}
        onCancel={() => setVisible(false)}>
        {visible && <LoginRegisterBox />}
      </Modal>
    </>
  );
}

export default withRouter(LoginRegister);
