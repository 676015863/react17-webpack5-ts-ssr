import React from 'react';
React.useLayoutEffect = React.useEffect;
import { StaticRouter } from 'react-router-dom';
import { renderToString } from 'react-dom/server';
import { stores } from './stores/index'; // ...
import { Provider } from 'mobx-react';
import { injectSSRDataToScripts } from '@ssr/ssrsdk';

import App from './App';
import { routes } from './routes.config';

// renderToHTML 不能修改原始routes
async function renderToHTML(route, props) {
  const scripts = await injectSSRDataToScripts(route);
  const shtml = renderToString(
    <Provider {...stores}>
      <App Router={StaticRouter} routes={routes} location={props.location} context={props.context} />
    </Provider>
  );
  return { shtml, scripts };
}

export { routes, renderToHTML };
