// import styles from './index-layout.module.less';

import React, { useEffect } from 'react';
import { renderRoutes } from 'react-router-config';
import { checkAuth } from '@utils/checkAuth';
import { userService } from '@server/index';
import PageLoading from '@components/page-loading';
import { inject, observer } from 'mobx-react';
import { Spin } from 'antd';
import Header from '@components/header';

function IndexLayout({ route, user }) {
  useEffect(() => {
    if (user.token && !user.info) {
      userService.getUserDetail();
    }
  }, []);

  if (checkAuth() && user.token && !user.info) {
    return <Spin />;
  }

  return (
    <>
      <Header type={'home'}/>
      <PageLoading />
      {renderRoutes(route.routes)}
    </>
  );
}
export default inject('user')(observer(IndexLayout));
