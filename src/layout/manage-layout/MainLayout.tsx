import styles from './mainlayout.module.less';

import React, { useEffect } from 'react';
import { renderRoutes } from 'react-router-config';
import { checkAuth } from '@utils/checkAuth';
import { userService } from '@server';
import PageLoading from '@components/page-loading';
import { inject, observer } from 'mobx-react';
import { Spin } from 'antd';
import Header from '@components/header';

function MainLayout({ route, user }) {
  useEffect(() => {
    if (checkAuth() && !user.info?.id) {
      userService.getUserDetail();
    }
  }, []);

  if (checkAuth() && !user.info?.id) {
    return <Spin />;
  }

  return (
    <>
      <Header type={'manage'} />
      <PageLoading />
      <div className={styles.manage}>{renderRoutes(route.routes)}</div>
    </>
  );
}
export default inject('user')(observer(MainLayout));
