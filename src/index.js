import './less/initialize.less';
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'mobx-react';
import App from './App';
import { stores } from './stores';
import { ssrInit } from '@ssr/ssrsdk';
import { BrowserRouter } from 'react-router-dom'; // 路由
import { routes } from './routes.config';
import 'antd/dist/antd.less';

ssrInit();

ReactDOM.render(
  <Provider {...stores}>
    <App Router={BrowserRouter} routes={routes} />
  </Provider>,
  document.getElementById('App')
);

if (module && module.hot) {
  module.hot.accept();
}
