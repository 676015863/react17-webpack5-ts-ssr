import { dynamic } from '@utils/dynamic';
const Home = dynamic(import('./Home'));

const routes = [
  {
    path: '/home',
    ssr: true,
    exact: true,
    component: Home
  }
];

export { routes };
