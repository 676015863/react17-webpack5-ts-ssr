import React, { Component } from 'react';
import styles from './home.module.less';
import { inject, observer } from 'mobx-react';
import { Tag, Skeleton, Button } from 'antd'; // ...
import { userService } from '@server';
import { withRouter } from 'react-router-dom';
import ListStatus from '@components/list-status';
import { demoService } from '@server';
import { getSSRData } from '@ssr/ssrsdk';
import Login from '@components/login';

/**
 * 使用 getSSRData 高性能的写法，避免二次请求数据
 */

// ssr 配置
// console.log(process.env.NODE_ENV);
@withRouter
@inject('user')
@observer
class Home extends Component {
  // 服务端注入数据到props
  static injectSSRData = async routeParams => {
    console.warn('ajax异步加载数据XXX', routeParams);
    const res = await demoService.getTemplates();
    return { ssrRes: res.data };
  };

  constructor(props) {
    super(props);
    this.staticSSRData = getSSRData();
    this.state = {
      list: props.ssrRes || this.staticSSRData?.ssrRes || null,
      captcha: userService.getCaptcha()
    };
  }

  componentDidMount() {
    if (!this.staticSSRData) {
      Home.injectSSRData().then(res => {
        console.log('ssrRes.data', res);
        this.setState({ list: res.ssrRes });
      });
    }
  }

  handleSubmit = async values => {
    // Toast.info('表单已提交');
    const res = await userService.login(values);
    console.log(values, res);
    if (res) {
      await userService.getUserDetail();
      this.props.history.push('/manage');
    }
  };

  render() {
    console.log(this.props.user);

    return (
      <>
        <div className={styles.home}>
          <div>
            <h1>SSR</h1>
            <div>
              <ListStatus data={this.state.list}>
                <Skeleton active style={{ width: 240 }} loading={!this.state.list}>
                  {this.state.list?.map(d => {
                    return (
                      <Tag style={{ margin: 5 }} key={d.id}>
                        {d.name}
                      </Tag>
                    );
                  })}
                </Skeleton>
              </ListStatus>
            </div>
          </div>
        </div>
        <div className={styles.home}>
          <Login>
            <Button type="primary">登录</Button>
          </Login>
        </div>
      </>
    );
  }
}
export default Home;
