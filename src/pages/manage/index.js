import { dynamic } from '../../utils';

const Manage = dynamic(import('./Manage'));

const routes = [{ path: '/manage', meta: { auth: true }, exact: true, component: Manage }];

export { routes };
