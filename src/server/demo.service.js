import BasicService from './BasicService';

class Demo extends BasicService {
  async getTemplates(page = 1, pageSize = 20) {
    return await this.get(
      `https://www.h5ds.com/api/v1/open/templates?type=0&page_size=${pageSize}&categoryId=&price_type=&order_column=is_top&industry_id=&color_id=&keyword=&page=${page}`,
      {
        headers: { Authorization: '' }
      }
    );
  }
}

export const demoService = new Demo();
