import Loadable from 'react-loadable';
import React from 'react';
import { Spin } from 'antd';

// if (props.error) {
//   return <div>Error!</div>;
// } else if (props.timedOut) {
//   return <div>Taking a long time...</div>;
// } else if (props.pastDelay) {
//   return <div>Loading...</div>;
// } else {
//   return null;
// }

function Loading({ error }: any) {
  if (error) {
    console.error(error);
    return <div>页面加载错误 </div>;
  }
  return <Spin />;
}
export function dynamic(compPromise: any) {
  return Loadable({
    //@ts-ignore
    loader: () => {
      return new Promise(resolve => {
        compPromise.then((obj: any) => {
          resolve(obj.default);
        });
      }).catch(err => {
        console.error('err', err);
      });
    },
    loading: Loading,
    timeout: 10000
  });
}
