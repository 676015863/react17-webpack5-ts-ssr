// import { routes as authorizationRoutes } from './pages/authorization';

import { routes as homeRoutes } from './pages/home';
import { routes as manageRoutes } from './pages/manage';
import { dynamic } from './utils';
// 管理页面
// const NotFound = dynamic(import('./components/not-found'));
const IndexLayout = dynamic(import('./layout/index-layout'));
const ManageLayout = dynamic(import('./layout/manage-layout'));

const routes = [
  {
    path: '/manage',
    exact: false,
    component: ManageLayout,
    routes: [...manageRoutes]
  },
  {
    path: '/',
    exact: false,
    component: IndexLayout,
    meta: { auth: false },
    routes: [
      ...homeRoutes
      // { path: '*', exact: false, component: NotFound }
    ]
  }
];

export { routes };
