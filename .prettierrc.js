module.exports = {
  parser: 'babel',
  printWidth: 120,
  semi: true,
  trailingComma: 'all',
  singleQuote: true,
  jsxBracketSameLine: true,
  arrowParens: 'avoid',
  trailingComma: 'none',
  tabWidth: 2
};
