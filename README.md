# 路由跳转

window.RouterHistory.push('/')

# webpack5 + react17 + antd + ssr 脚手架

ui库采用：https://ant-design.gitee.io/index-cn

> npm install 安装依赖包

> npm start 启动项目

> npm run build 打包项目

如果希望打包后资源文件名不被修改，放到publish/assets中

如果希望打包后资源文件名自动修改，放到src/source中

# 环境变量

### 开发环境
> process.env.NODE_ENV === 'development'

### 正式环境
> process.env.NODE_ENV === 'production'

# hash配置

如果hash配置关闭了，需要注意一下`publish/assets` 和 `source/assets` 文件名需要避免冲突

# 手机页面

如果制作手机端页面，使用postcss-px-to-viewport

# 其他说明

### 目录说明

```javascript

-dist                                  打包目录
-publich                               公共资源目录
-scripts                               webpack脚本配置
-ssr                                   ssr server相关业务代码
-src                                   源码目录
  -components                          公共组件
  -config                              配置信息
  -language                            国际化配置
  -layout                              框架
  -less                                公共less
  -pages                               页面模块
  -server                              api服务
  -assets                              assets资源
  -stores                              mobx的store
  -utils                               插件
  index.js                             入口
  ssr.js                               ssr入口
  App.js                               入口组件
  routes.config.js                     路由配置
```

### 路由说明

```javascript

meta: { auth: true, ssr: true }

auth true 表示是否需要登录才能访问，路由配置放到routes.config.js中，且必须放到 '/' 配置之前

ssr true 表示该路由需要做SSR

参考：https://gitee.com/676015863/react17-webpack5-ts-ssr/blob/master/src/pages/home/index.js

```

### 用户信息

```javascript

用户数据放到了mobx中进行管理，通过user.info（:proxy）可以拿到

```

