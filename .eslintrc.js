module.exports = {
  parser: '@babel/eslint-parser', // '@typescript-eslint/parser', // 指定ESLint解析器
  plugins: ['react', 'react-hooks'],
  extends: [
    'plugin:react/recommended', // 使用来自 @eslint-plugin-react 的推荐规则
    'plugin:@typescript-eslint/recommended', // 使用来自@typescript-eslint/eslint-plugin的推荐规则
    'plugin:prettier/recommended',
    'prettier'
  ],
  parserOptions: {
    ecmaVersion: 2018, // 允许解析最新的 ECMAScript 特性
    sourceType: 'module', // 允许使用 import
    ecmaFeatures: {
      jsx: true // 允许对JSX进行解析
    }
  },
  rules: {
    semi: 'warn',
    'no-console': 'off',
    'react/prefer-es6-class': 'off',
    'react/prop-types': 'off',
    'react-hooks/rules-of-hooks': 'error',
    'react-hooks/exhaustive-deps': 'warn'
  },
  settings: {
    react: {
      version: 'detect' // 告诉 eslint-plugin-react 自动检测 React 的版本
    }
  },
  env: {
    es6: true,
    browser: true,
    node: true,
    jquery: true,
    commonjs: true
  },
  globals: {
    window: true, //xxxx -> 报错的变量
    JSX: true
  }
};
